package com.online.book.store.application.project.Exception;

import com.online.book.store.application.project.Dto.ResponseObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ResponseObject> resourceNotFoundExceptionHandler(ResourceNotFoundException exception) {
        ResponseObject responseObject = new ResponseObject();

        responseObject.setMessage(exception.getMessage());
        responseObject.setStatus(false);
        responseObject.setData(null);

        return new ResponseEntity<>(responseObject, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(FileTypeIncorrectException.class)
    public ResponseEntity<ResponseObject> fileTypeIncorrectExceptionHandler(FileTypeIncorrectException exception) {
        ResponseObject responseObject = new ResponseObject();

        responseObject.setMessage(exception.getMessage());
        responseObject.setStatus(false);
        responseObject.setData(null);

        return new ResponseEntity<>(responseObject, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BookOutOfStockException.class)
    public ResponseEntity<ResponseObject> bookOutOfStockExceptionHandler(BookOutOfStockException exception) {
        ResponseObject responseObject = new ResponseObject();

        responseObject.setMessage(exception.getMessage());
        responseObject.setStatus(false);
        responseObject.setData(null);

        return new ResponseEntity<>(responseObject, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ResponseObject> apiExceptionHandler(ApiException exception) {
        ResponseObject responseObject = new ResponseObject();

        responseObject.setMessage(exception.getMessage());
        responseObject.setStatus(false);
        responseObject.setData(null);

        return new ResponseEntity<>(responseObject, HttpStatus.BAD_REQUEST);
    }
}
