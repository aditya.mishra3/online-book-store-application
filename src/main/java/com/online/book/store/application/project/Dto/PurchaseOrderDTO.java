package com.online.book.store.application.project.Dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class PurchaseOrderDTO {

    private String orderId;
    private String userId;
    private long orderDate;
    private String paymentStatus;
    private Integer orderTotal;
    private List<String> bookInfo;

}
