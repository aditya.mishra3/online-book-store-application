package com.online.book.store.application.project.Controller;

import com.online.book.store.application.project.Dto.PurchaseOrderDTO;
import com.online.book.store.application.project.Dto.ResponseObject;
import com.online.book.store.application.project.Service.PurchaseOrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/purchaseOrder")
public class PurchaseOrderController {

    @Autowired
    private PurchaseOrderServiceImpl purchaseOrderService;

    @PostMapping("/create")
    public ResponseEntity<ResponseObject> createPurchaseOrder(@RequestBody PurchaseOrderDTO purchaseOrderDTO) {
        ResponseObject responseObject = new ResponseObject();

        PurchaseOrderDTO purchaseOrderDto = purchaseOrderService.createPurchaseOrder(purchaseOrderDTO);

        responseObject.setMessage("Purchase Order Successfully Created");
        responseObject.setStatus(true);
        responseObject.setData(purchaseOrderDto);

        return new ResponseEntity<>(responseObject, HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<ResponseObject> getAllPurchaseOrders() {
        ResponseObject responseObject = new ResponseObject();

        List<PurchaseOrderDTO> purchaseOrderDTOList = purchaseOrderService.getAllPurchaseOrders();

        responseObject.setMessage("All Purchase Order Fetched");
        responseObject.setStatus(true);
        responseObject.setData(purchaseOrderDTOList);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<ResponseObject> getPurchaseOrderByOrderId(@PathVariable String orderId) {
        ResponseObject responseObject = new ResponseObject();

        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderService.findByPurchaseOrderId(orderId);

        responseObject.setMessage("Purchase Order With Id: " + orderId + " Fetched Successfully");
        responseObject.setStatus(true);
        responseObject.setData(purchaseOrderDTO);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/export/excel/daily")
    public void getDailyPurchaseOrderData(HttpServletResponse response, @RequestParam(value = "day") int day, @RequestParam(value = "month") int month, @RequestParam(value = "year") int year) throws IOException {
        purchaseOrderService.getDailyPurchaseOrderData(response, day, month, year);
    }

    @GetMapping("/export/excel/weekly")
    public void getWeeklyPurchaseOrderData(HttpServletResponse response, @RequestParam(value = "weekNumber") int weekNumber, @RequestParam(value = "year") int year) throws IOException {
        purchaseOrderService.getWeeklyPurchaseOrderData(response, weekNumber, year);
    }

    @GetMapping("/export/excel/monthly")
    public void getMonthlyPurchaseOrderData(HttpServletResponse response, @RequestParam(value = "month") int month, @RequestParam(value = "year") int year) throws IOException {
        purchaseOrderService.getMonthlyPurchaseOrderData(response, month, year);
    }

    @GetMapping("/export/excel/yearly")
    public void getYearlyPurchaseOrderData(HttpServletResponse response, @RequestParam(value = "year") int year) throws IOException {
        purchaseOrderService.getYearlyPurchaseOrderData(response, year);
    }
}
