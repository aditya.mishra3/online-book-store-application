package com.online.book.store.application.project.Controller;

import com.online.book.store.application.project.Dto.ResponseObject;
import com.online.book.store.application.project.Model.BookSampleFile;
import com.online.book.store.application.project.Service.BookSampleFileServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/file")
public class BookSampleFileController {

    @Value("${project.book_sample}")
    private String path;

    @Autowired
    private BookSampleFileServiceImpl bookSampleFileService;

    @PostMapping("/updateFile")
    public ResponseEntity<ResponseObject> uploadBookSample(@RequestParam("file") MultipartFile file) throws IOException {
        ResponseObject responseObject = new ResponseObject();

        if(file.isEmpty()) {
            responseObject.setMessage("File Not Found");
            responseObject.setStatus(false);
            responseObject.setData(null);
        }
        else {
            String pdfId = bookSampleFileService.uploadAndUpdateBookSampleFile(file, path);

            responseObject.setStatus(true);
            responseObject.setMessage("File Uploaded Successfully");
            responseObject.setData(pdfId);
        }

        return ResponseEntity.ok(responseObject);
    }

    @GetMapping("/download/{id}")
    public ResponseEntity<ByteArrayResource> download(@PathVariable String id) throws IOException{

        BookSampleFile loadFile = bookSampleFileService.downloadBookSampleFile(id);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(loadFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + loadFile.getFilename() + "\"")
                .body(new ByteArrayResource(loadFile.getFile()));
    }
}
