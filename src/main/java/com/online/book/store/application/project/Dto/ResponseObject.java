package com.online.book.store.application.project.Dto;

import lombok.Data;

@Data
public class ResponseObject {

    private String message;
    private boolean status;
    private Object data;

}
