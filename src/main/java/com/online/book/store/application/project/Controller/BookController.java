package com.online.book.store.application.project.Controller;

import com.online.book.store.application.project.Dto.BookDTO;
import com.online.book.store.application.project.Dto.ResponseObject;
import com.online.book.store.application.project.Service.BookServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/book")
public class BookController {

    @Autowired
    private BookServiceImpl bookService;

    @PostMapping("/create")
    public ResponseEntity<ResponseObject> createBook(@RequestBody BookDTO bookDTO) {
        ResponseObject responseObject = new ResponseObject();

        bookDTO.setBook_sample("null");
        bookDTO.setInStock(true);
        BookDTO bookDto = bookService.createBook(bookDTO);

        responseObject.setMessage("Book Successfully Created");
        responseObject.setStatus(true);
        responseObject.setData(bookDto);

        return new ResponseEntity<>(responseObject, HttpStatus.CREATED);
    }

    @PutMapping("/update/{bookId}")
    public ResponseEntity<ResponseObject> updateBook(@RequestBody BookDTO bookDTO, @PathVariable String bookId) {
        ResponseObject responseObject = new ResponseObject();

        BookDTO bookDto = bookService.updateBook(bookDTO, bookId);

        responseObject.setMessage("Book With Id: " + bookId + " Updated Successfully");
        responseObject.setStatus(true);
        responseObject.setData(bookDto);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{bookId}")
    public ResponseEntity<ResponseObject> deleteBook(@RequestBody BookDTO bookDTO, @PathVariable String bookId) throws IOException {
        ResponseObject responseObject = new ResponseObject();

        long deletedBook = bookService.deleteBook(bookDTO, bookId);

        responseObject.setMessage("Book With Id: " + bookId + " Deleted Successfully");
        responseObject.setStatus(true);
        responseObject.setData(deletedBook);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<ResponseObject> getAllBooks() {
        ResponseObject responseObject = new ResponseObject();

        List<BookDTO> bookDTOList = bookService.getAllBooks();

        responseObject.setMessage("All Books Fetched");
        responseObject.setStatus(true);
        responseObject.setData(bookDTOList);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/{bookId}")
    public ResponseEntity<ResponseObject> getBookByBookId(@PathVariable String bookId) {
        ResponseObject responseObject = new ResponseObject();

        BookDTO bookDTO = bookService.findByBookId(bookId);

        responseObject.setMessage("Book With Id: " + bookId + " Fetched Successfully");
        responseObject.setStatus(true);
        responseObject.setData(bookDTO);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
