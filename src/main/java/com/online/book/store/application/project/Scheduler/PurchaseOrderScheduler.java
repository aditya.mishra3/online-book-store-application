package com.online.book.store.application.project.Scheduler;

import com.online.book.store.application.project.Exception.ResourceNotFoundException;
import com.online.book.store.application.project.Model.Book;
import com.online.book.store.application.project.Model.PurchaseOrder;
import com.online.book.store.application.project.Model.PurchaseOrderInfoByPeriod;
import com.online.book.store.application.project.Model.User;
import com.online.book.store.application.project.Repository.BookRepository;
import com.online.book.store.application.project.Repository.PurchaseOrderInfoByPeriodRepository;
import com.online.book.store.application.project.Repository.PurchaseOrderRepository;
import com.online.book.store.application.project.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component
public class PurchaseOrderScheduler {

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private PurchaseOrderInfoByPeriodRepository purchaseOrderInfoByPeriodRepository;

    @Scheduled(cron = "0 5 0 * * *")
    public void checkDailyPurchaseOrderData() {
        long endDate = System.currentTimeMillis();
        long oneDayInMilli = 24 * 60 * 60 * 1000;
        long startDate = endDate - oneDayInMilli;

        List<PurchaseOrder> purchaseOrderList = purchaseOrderRepository.findPurchaseOrderBetweenOrderDate(startDate, endDate);

        for (PurchaseOrder purchaseOrder : purchaseOrderList) {
            PurchaseOrderInfoByPeriod purchaseOrderInfoByPeriod = new PurchaseOrderInfoByPeriod();
            User user = userRepository.findByUserId(purchaseOrder.getUserId()).orElseThrow(() -> new ResourceNotFoundException("User", "ID", purchaseOrder.getUserId()));
            purchaseOrderInfoByPeriod.setUserName(user.getName());
            purchaseOrderInfoByPeriod.setTotalAmount(purchaseOrder.getOrderTotal());
            List<String> bookName = new ArrayList<>();
            for (String bookId : purchaseOrder.getBookInfo()) {
                Book book = bookRepository.findByBookId(bookId).orElseThrow(() -> new ResourceNotFoundException("Book", "ID", bookId));

                bookName.add(book.getName());
            }

            purchaseOrderInfoByPeriod.setBookName(bookName);
            LocalDateTime ldt = LocalDateTime.from(Instant.ofEpochMilli(purchaseOrder.getOrderDate()).atZone(ZoneId.of("Asia/Kolkata")));

            purchaseOrderInfoByPeriod.setDay(ldt.getDayOfMonth());
            purchaseOrderInfoByPeriod.setMonth(ldt.getMonthValue());
            purchaseOrderInfoByPeriod.setYear(ldt.getYear());
            Integer weekNumber = getCurrentWeekNumber(ldt);
            purchaseOrderInfoByPeriod.setWeekNumber(weekNumber);
            purchaseOrderInfoByPeriod.setDateTime(purchaseOrder.getOrderDate());

            purchaseOrderInfoByPeriodRepository.createPurchaseOrderInfoByPeriod(purchaseOrderInfoByPeriod);
        }
    }

    public Integer getCurrentWeekNumber(LocalDateTime ldt) {
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        return ldt.get(weekFields.weekOfWeekBasedYear());
    }
}
