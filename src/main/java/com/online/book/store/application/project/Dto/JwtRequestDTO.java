package com.online.book.store.application.project.Dto;

import lombok.Data;

@Data
public class JwtRequestDTO {

    private String username;
    private String password;

}
