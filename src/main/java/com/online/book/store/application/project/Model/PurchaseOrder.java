package com.online.book.store.application.project.Model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

@Data
@Document(collection = "Purchase_details")
public class PurchaseOrder {

    @Id
    private String orderId;
    private String userId;
    private long orderDate;
    private String paymentStatus;
    private Integer orderTotal;
    private List<String> bookInfo;

}
