package com.online.book.store.application.project.Dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserDTO {

    private String userId;
    private String name;
    private String email;
    private String password;
    private String address;
    private String phoneNumber;
    private List<RoleDTO> roles = new ArrayList<>();

}
