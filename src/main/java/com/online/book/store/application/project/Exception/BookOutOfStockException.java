package com.online.book.store.application.project.Exception;

public class BookOutOfStockException extends RuntimeException {

    public BookOutOfStockException() {
        super("Book is Out of Stock Now. We will notify you when it's back in Stock");
    }
}
