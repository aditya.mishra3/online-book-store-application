package com.online.book.store.application.project.Controller;

import com.online.book.store.application.project.Dto.OwnerDTO;
import com.online.book.store.application.project.Dto.ResponseObject;
import com.online.book.store.application.project.Service.OwnerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/owner")
public class OwnerController {

    @Autowired
    private OwnerServiceImpl ownerService;

    @PostMapping("/create")
    public ResponseEntity<ResponseObject> createOwner(@RequestBody OwnerDTO ownerDTO) {
        ResponseObject responseObject = new ResponseObject();

        OwnerDTO ownerDto = ownerService.createOwner(ownerDTO);

        responseObject.setMessage("Owner Successfully Created");
        responseObject.setStatus(true);
        responseObject.setData(ownerDto);

        return new ResponseEntity<>(responseObject, HttpStatus.CREATED);
    }

    @PutMapping("/update/{ownerId}")
    public ResponseEntity<ResponseObject> updateOwner(@RequestBody OwnerDTO ownerDTO, @PathVariable String ownerId) {
        ResponseObject responseObject = new ResponseObject();

        OwnerDTO ownerDto = ownerService.updateOwner(ownerDTO, ownerId);

        responseObject.setMessage("Owner With Id: " + ownerId + " Updated Successfully");
        responseObject.setStatus(true);
        responseObject.setData(ownerDto);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{ownerId}")
    public ResponseEntity<ResponseObject> deleteOwner(@RequestBody OwnerDTO ownerDTO, @PathVariable String ownerId) {
        ResponseObject responseObject = new ResponseObject();

        long deletedOwner = ownerService.deleteOwner(ownerDTO, ownerId);

        responseObject.setMessage("Owner With Id: " + ownerId + " Deleted Successfully");
        responseObject.setStatus(true);
        responseObject.setData(deletedOwner);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<ResponseObject> getAllOwners() {
        ResponseObject responseObject = new ResponseObject();

        List<OwnerDTO> ownerDTOList = ownerService.getAllOwners();

        responseObject.setMessage("All Owners Fetched");
        responseObject.setStatus(true);
        responseObject.setData(ownerDTOList);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/{ownerId}")
    public ResponseEntity<ResponseObject> getOwnerByOwnerId(@PathVariable String ownerId) {
        ResponseObject responseObject = new ResponseObject();

        OwnerDTO ownerDto = ownerService.findByOwnerId(ownerId);

        responseObject.setMessage("Owner With Id: " + ownerId + " Fetched Successfully");
        responseObject.setStatus(true);
        responseObject.setData(ownerDto);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
