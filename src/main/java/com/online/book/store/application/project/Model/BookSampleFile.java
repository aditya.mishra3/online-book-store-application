package com.online.book.store.application.project.Model;

import lombok.Data;

@Data
public class BookSampleFile {

    private String filename;
    private String fileType;
    private String fileSize;
    private byte[] file;

}
