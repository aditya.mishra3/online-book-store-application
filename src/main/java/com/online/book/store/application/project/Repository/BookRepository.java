package com.online.book.store.application.project.Repository;

import com.online.book.store.application.project.Model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class BookRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    public Book createBook(Book book) {
        return mongoTemplate.save(book);
    }

    public long deleteBook(Book book) {
        return mongoTemplate.remove(book).getDeletedCount();
    }

    public List<Book> getAllBooks() {
        List<Book> books = mongoTemplate.findAll(Book.class);
        return books;
    }

    public Optional<Book> findByBookId(String bookId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("bookId").is(bookId));
        return mongoTemplate.find(query, Book.class).stream().findFirst();
    }

    public Optional<Book> findByBookName(String bookName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(bookName));
        return mongoTemplate.find(query, Book.class).stream().findFirst();
    }
}
