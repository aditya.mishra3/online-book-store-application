package com.online.book.store.application.project.Repository;

import com.online.book.store.application.project.Model.Owner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class OwnerRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    public Owner createOwner(Owner owner) {
        return mongoTemplate.save(owner);
    }

    public long deleteOwner(Owner owner) {
        return mongoTemplate.remove(owner).getDeletedCount();
    }

    public List<Owner> getAllOwners() {
        List<Owner> owners = mongoTemplate.findAll(Owner.class);
        return owners;
    }

    public Optional<Owner> findByOwnerId(String ownerId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(ownerId));
        return mongoTemplate.find(query, Owner.class).stream().findFirst();
    }

    public Optional<Owner> findByEmail(String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(email));
        return mongoTemplate.find(query, Owner.class).stream().findFirst();
    }
}
