package com.online.book.store.application.project.Service;

import com.online.book.store.application.project.Dto.BookDTO;
import com.online.book.store.application.project.Dto.WarehouseDTO;
import com.online.book.store.application.project.Exception.ResourceNotFoundException;
import com.online.book.store.application.project.Impl.WarehouseService;
import com.online.book.store.application.project.Model.Owner;
import com.online.book.store.application.project.Model.Warehouse;
import com.online.book.store.application.project.Repository.OwnerRepository;
import com.online.book.store.application.project.Repository.WarehouseRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public WarehouseDTO createWarehouse(WarehouseDTO warehouseDTO) {
        Warehouse warehouse = modelMapper.map(warehouseDTO, Warehouse.class);
        Warehouse savedWarehouse = warehouseRepository.createWarehouse(warehouse);
        WarehouseDTO warehouseDto = modelMapper.map(savedWarehouse, WarehouseDTO.class);
        ownerRepository.getAllOwners().stream().forEach((owner) -> {
            owner.getWarehouses().add(warehouseDto.getWarehouseId());
            ownerRepository.createOwner(owner);
        });
        return warehouseDto;
    }

    @Override
    public WarehouseDTO updateWarehouse(WarehouseDTO warehouseDTO, String warehouseId) {
        Warehouse warehouse = warehouseRepository.findByWarehouseId(warehouseId).orElseThrow(() -> new ResourceNotFoundException("Warehouse", "ID", warehouseId));

        warehouse.setAddress(warehouseDTO.getAddress());
        warehouse.setPhone(warehouseDTO.getPhone());
        warehouse.setBooksDetails(warehouseDTO.getBooksDetails());

        Warehouse updatedWarehouse = warehouseRepository.createWarehouse(warehouse);

        return modelMapper.map(updatedWarehouse, WarehouseDTO.class);
    }

    @Override
    public long deleteWarehouse(WarehouseDTO WarehouseDTO, String warehouseId) {
        Warehouse warehouse = warehouseRepository.findByWarehouseId(warehouseId).orElseThrow(() -> new ResourceNotFoundException("Warehouse", "ID", warehouseId));

        long deletedWarehouse = warehouseRepository.deleteWarehouse(warehouse);
        Owner owner = ownerRepository.getAllOwners().get(0);
        owner.getWarehouses().remove(warehouseId);
        ownerRepository.createOwner(owner);

        return deletedWarehouse;
    }

    @Override
    public List<WarehouseDTO> getAllWarehouse() {
        List<Warehouse> warehouseList = warehouseRepository.getAllWarehouses();

        return warehouseList.stream().map((warehouse) -> modelMapper.map(warehouse, WarehouseDTO.class)).collect(Collectors.toList());
    }

    @Override
    public WarehouseDTO findByWarehouseId(String warehouseId) {
        Warehouse warehouse = warehouseRepository.findByWarehouseId(warehouseId).orElseThrow(() -> new ResourceNotFoundException("Warehouse", "Id", warehouseId));

        return modelMapper.map(warehouse, WarehouseDTO.class);
    }
}
