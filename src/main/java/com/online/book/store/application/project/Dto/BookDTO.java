package com.online.book.store.application.project.Dto;

import lombok.Data;

@Data
public class BookDTO {

    private String bookId;
    private String author;
    private String name;
    private Integer price;
    private String book_sample;
    private boolean inStock;

}
