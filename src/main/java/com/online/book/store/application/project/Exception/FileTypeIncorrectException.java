package com.online.book.store.application.project.Exception;

public class FileTypeIncorrectException extends RuntimeException{

    public FileTypeIncorrectException() {
        super("File Type is not correct. please provide file with type: PDF");
    }
}
