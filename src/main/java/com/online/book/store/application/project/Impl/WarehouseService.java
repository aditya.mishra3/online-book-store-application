package com.online.book.store.application.project.Impl;

import com.online.book.store.application.project.Dto.WarehouseDTO;

import java.util.List;

public interface WarehouseService {

    WarehouseDTO createWarehouse(WarehouseDTO warehouseDTO);

    WarehouseDTO updateWarehouse(WarehouseDTO warehouseDTO, String warehouseId);

    long deleteWarehouse(WarehouseDTO WarehouseDTO, String warehouseId);

    List<WarehouseDTO> getAllWarehouse();

    WarehouseDTO findByWarehouseId(String warehouseId);

}
