package com.online.book.store.application.project.Service;

import com.online.book.store.application.project.Dto.UserDTO;
import com.online.book.store.application.project.Exception.ResourceNotFoundException;
import com.online.book.store.application.project.Impl.UserService;
import com.online.book.store.application.project.Model.Role;
import com.online.book.store.application.project.Model.User;
import com.online.book.store.application.project.Repository.RoleRepository;
import com.online.book.store.application.project.Repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public UserDTO createUser(UserDTO userDto) {
        User user = modelMapper.map(userDto, User.class);

        user.setPassword(passwordEncoder.encode(userDto.getPassword()));

        Role userRole = roleRepository.findByRoleId(71).orElseThrow(() -> new ResourceNotFoundException("Role", "ID", "71"));

        user.getRoles().add(userRole);

        User savedUSer = userRepository.createUser(user);

        return modelMapper.map(savedUSer, UserDTO.class);
    }

    @Override
    public UserDTO updateUser(UserDTO userDto, String userId) {
        User user = userRepository.findByUserId(userId).orElseThrow(() -> new ResourceNotFoundException("User", "ID", userId));

        user.setName(userDto.getName());
        user.setAddress(userDto.getAddress());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setPhoneNumber(userDto.getPhoneNumber());

        User updatedUser = userRepository.createUser(user);

        return modelMapper.map(user, UserDTO.class);
    }

    @Override
    public long deleteUser(UserDTO userDto, String userId) {
        User user = userRepository.findByUserId(userId).orElseThrow(() -> new ResourceNotFoundException("User", "ID", userId));

        return userRepository.deleteUser(user);
    }

    @Override
    public List<UserDTO> getAllUsers() {
        List<User> userList = userRepository.getAllUsers();
        return userList.stream().map((user) -> modelMapper.map(user, UserDTO.class)).collect(Collectors.toList());
    }

    @Override
    public UserDTO findByUserId(String userId) {
        User user = userRepository.findByUserId(userId).orElseThrow(() -> new ResourceNotFoundException("User", "Id", userId));

        return modelMapper.map(user, UserDTO.class);
    }
}
