package com.online.book.store.application.project.Util;

import com.online.book.store.application.project.Exception.ResourceNotFoundException;
import com.online.book.store.application.project.Model.Book;
import com.online.book.store.application.project.Repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmailUtil {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private BookRepository bookRepository;

    @Value("${spring.mail.username}")
    private String sentFrom;

    public void sendEmailToUser(String toEmail, String body, String subject, String attachment) throws MessagingException {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

        mimeMessageHelper.setFrom(sentFrom);
        mimeMessageHelper.setTo(toEmail);
        mimeMessageHelper.setText(body);
        mimeMessageHelper.setSubject(subject);

        FileSystemResource fileSystemResource = new FileSystemResource(new File(attachment));

        mimeMessageHelper.addAttachment(fileSystemResource.getFilename(), fileSystemResource);

        javaMailSender.send(mimeMessage);
        System.out.println("Mail Sent....");
    }

    public void sendEmailToOwner(String toEmail, String subject, List<String> bookIds, String ownerName) {

        List<String> bookName = new ArrayList<>();
        for (String bookId : bookIds) {
            Book book = bookRepository.findByBookId(bookId).orElseThrow(() -> new ResourceNotFoundException("Book", "ID", bookId));

            bookName.add(book.getName());
        }

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

        String mailBody = "Hello " + ownerName + ",\n"
                + "Books With Following Name: " + bookName + " has Less Quantity." + "\n"
                + "Please place the order of the given books" + "\n"
                + "Thank you.";

        simpleMailMessage.setFrom(sentFrom);
        simpleMailMessage.setTo(toEmail);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(mailBody);

        javaMailSender.send(simpleMailMessage);
        System.out.println("Mail Sent....");
    }
}
