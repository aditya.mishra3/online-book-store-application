package com.online.book.store.application.project.Repository;

import com.online.book.store.application.project.Model.PurchaseOrderInfoByPeriod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseOrderInfoByPeriodRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    public PurchaseOrderInfoByPeriod createPurchaseOrderInfoByPeriod(PurchaseOrderInfoByPeriod purchaseOrderInfoByPeriod) {
        return mongoTemplate.save(purchaseOrderInfoByPeriod);
    }

    public long deletePurchaseOrderInfoByPeriod(PurchaseOrderInfoByPeriod purchaseOrderInfoByPeriod) {
        return mongoTemplate.remove(purchaseOrderInfoByPeriod).getDeletedCount();
    }

    public List<PurchaseOrderInfoByPeriod> getAllPurchaseOrdersInfoByPeriod() {
        List<PurchaseOrderInfoByPeriod> purchaseOrderInfoByPeriods = mongoTemplate.findAll(PurchaseOrderInfoByPeriod.class);
        return purchaseOrderInfoByPeriods;
    }

    public Optional<PurchaseOrderInfoByPeriod> findByPurchaseOrderInfoByPeriodId(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return mongoTemplate.find(query, PurchaseOrderInfoByPeriod.class).stream().findFirst();
    }

    public List<PurchaseOrderInfoByPeriod> getDailyPurchaseOrderData(int day, int month, int year) {
        Query query = new Query();
        query.addCriteria(Criteria.where("day").is(day));
        query.addCriteria(Criteria.where("month").is(month));
        query.addCriteria(Criteria.where("year").is(year));
        return mongoTemplate.find(query, PurchaseOrderInfoByPeriod.class);
    }

    public List<PurchaseOrderInfoByPeriod> getWeeklyPurchaseOrderData(int weekNumber, int year) {
        Query query = new Query();
        query.addCriteria(Criteria.where("weekNumber").is(weekNumber));
        query.addCriteria(Criteria.where("year").is(year));
        return mongoTemplate.find(query, PurchaseOrderInfoByPeriod.class);
    }

    public List<PurchaseOrderInfoByPeriod> getMonthlyPurchaseOrderData(int month, int year) {
        Query query = new Query();
        query.addCriteria(Criteria.where("month").is(month));
        query.addCriteria(Criteria.where("year").is(year));
        return mongoTemplate.find(query, PurchaseOrderInfoByPeriod.class);
    }

    public List<PurchaseOrderInfoByPeriod> getYearlyPurchaseOrderData(int year) {
        Query query = new Query();
        query.addCriteria(Criteria.where("year").is(year));
        return mongoTemplate.find(query, PurchaseOrderInfoByPeriod.class);
    }
}
