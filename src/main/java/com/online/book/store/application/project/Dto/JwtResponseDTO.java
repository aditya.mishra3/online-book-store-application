package com.online.book.store.application.project.Dto;

import lombok.Data;

@Data
public class JwtResponseDTO {

    private String token;

}
