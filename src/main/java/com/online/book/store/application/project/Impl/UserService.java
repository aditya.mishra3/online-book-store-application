package com.online.book.store.application.project.Impl;

import com.online.book.store.application.project.Dto.UserDTO;

import java.util.List;

public interface UserService {

    UserDTO createUser(UserDTO userDto);

    UserDTO updateUser(UserDTO userDto, String userId);

    long deleteUser(UserDTO userDto, String userId);

    List<UserDTO> getAllUsers();

    UserDTO findByUserId(String userId);

}
