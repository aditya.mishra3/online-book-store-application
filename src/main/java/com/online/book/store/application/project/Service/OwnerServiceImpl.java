package com.online.book.store.application.project.Service;

import com.online.book.store.application.project.Dto.OwnerDTO;
import com.online.book.store.application.project.Dto.UserDTO;
import com.online.book.store.application.project.Exception.ResourceNotFoundException;
import com.online.book.store.application.project.Impl.OwnerService;
import com.online.book.store.application.project.Model.Owner;
import com.online.book.store.application.project.Model.Role;
import com.online.book.store.application.project.Model.User;
import com.online.book.store.application.project.Repository.OwnerRepository;
import com.online.book.store.application.project.Repository.RoleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OwnerServiceImpl implements OwnerService {

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public OwnerDTO createOwner(OwnerDTO ownerDTO) {
        Owner owner = modelMapper.map(ownerDTO, Owner.class);

        owner.setPassword(passwordEncoder.encode(ownerDTO.getPassword()));

        Role ownerRole = roleRepository.findByRoleId(70).orElseThrow(() -> new ResourceNotFoundException("Role", "ID", "70"));

        owner.getRoles().add(ownerRole);

        Owner savedOwner = ownerRepository.createOwner(owner);

        return modelMapper.map(savedOwner, OwnerDTO.class);
    }

    @Override
    public OwnerDTO updateOwner(OwnerDTO ownerDTO, String ownerId) {
        Owner owner = ownerRepository.findByOwnerId(ownerId).orElseThrow(() -> new ResourceNotFoundException("Owner", "ID", ownerId));

        owner.setName(ownerDTO.getName());
        owner.setEmail(ownerDTO.getEmail());
        owner.setAddress(ownerDTO.getAddress());
        owner.setPassword(ownerDTO.getPassword());
        owner.setPhoneNumber(ownerDTO.getPhoneNumber());

        Owner updatedOwner = ownerRepository.createOwner(owner);
        return modelMapper.map(updatedOwner, OwnerDTO.class);
    }

    @Override
    public long deleteOwner(OwnerDTO ownerDTO, String ownerId) {
        Owner owner = ownerRepository.findByOwnerId(ownerId).orElseThrow(() -> new ResourceNotFoundException("Owner", "ID", ownerId));

        return ownerRepository.deleteOwner(owner);
    }

    @Override
    public List<OwnerDTO> getAllOwners() {
        List<Owner> owners = ownerRepository.getAllOwners();

        return owners.stream().map((owner) -> modelMapper.map(owner, OwnerDTO.class)).collect(Collectors.toList());
    }

    @Override
    public OwnerDTO findByOwnerId(String ownerId) {
        Owner owner = ownerRepository.findByOwnerId(ownerId).orElseThrow(() -> new ResourceNotFoundException("Owner", "ID", ownerId));

        return modelMapper.map(owner, OwnerDTO.class);
    }
}
