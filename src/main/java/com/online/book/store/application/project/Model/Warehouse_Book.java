package com.online.book.store.application.project.Model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "warehouse_book_details")
public class Warehouse_Book {

    private String bookId;
    private Integer quantity;

}
