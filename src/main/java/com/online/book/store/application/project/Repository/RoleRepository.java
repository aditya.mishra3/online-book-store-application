package com.online.book.store.application.project.Repository;

import com.online.book.store.application.project.Model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class RoleRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    public Role createRole(Role role) {
        return mongoTemplate.save(role);
    }

    public long deleteRole(Role role) {
        return mongoTemplate.remove(role).getDeletedCount();
    }

    public List<Role> getAllRoles() {
        List<Role> roles = mongoTemplate.findAll(Role.class);
        return roles;
    }

    public Optional<Role> findByRoleId(int roleId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(roleId));
        return mongoTemplate.find(query, Role.class).stream().findFirst();
    }
}
