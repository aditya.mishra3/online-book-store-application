package com.online.book.store.application.project.Model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "purchase_order_period_details")
public class PurchaseOrderInfoByPeriod {

    @Id
    private String id;
    private String userName;
    private Integer totalAmount;
    private List<String> bookName;

    private Integer day;
    private Integer weekNumber;
    private Integer month;
    private Integer year;
    private Long dateTime;

}
