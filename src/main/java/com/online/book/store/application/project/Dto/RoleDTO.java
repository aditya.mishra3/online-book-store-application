package com.online.book.store.application.project.Dto;

import lombok.Data;

@Data
public class RoleDTO {

    private int id;
    private String name;

}
