package com.online.book.store.application.project.Impl;

import com.online.book.store.application.project.Dto.PurchaseOrderDTO;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface PurchaseOrderService {

    PurchaseOrderDTO createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO);

    long deletePurchaseOrder(PurchaseOrderDTO purchaseOrderDTO, String orderId);

    List<PurchaseOrderDTO> getAllPurchaseOrders();

    PurchaseOrderDTO findByPurchaseOrderId(String orderId);

    void getDailyPurchaseOrderData(HttpServletResponse response, int day, int month, int year) throws IOException;

    void getWeeklyPurchaseOrderData(HttpServletResponse response, int weekNumber, int year) throws IOException;

    void getMonthlyPurchaseOrderData(HttpServletResponse response, int month, int year) throws IOException;

    void getYearlyPurchaseOrderData(HttpServletResponse response, int year) throws IOException;

}
