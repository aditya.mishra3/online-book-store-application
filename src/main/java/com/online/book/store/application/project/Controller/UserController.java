package com.online.book.store.application.project.Controller;

import com.mongodb.client.gridfs.model.GridFSFile;
import com.online.book.store.application.project.Dto.BookDTO;
import com.online.book.store.application.project.Dto.ResponseObject;
import com.online.book.store.application.project.Dto.UserDTO;
import com.online.book.store.application.project.Exception.ResourceNotFoundException;
import com.online.book.store.application.project.Service.BookServiceImpl;
import com.online.book.store.application.project.Service.UserServiceImpl;
import com.online.book.store.application.project.Util.EmailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private BookServiceImpl bookService;

    @Autowired
    private GridFsTemplate template;

    @Autowired
    private EmailUtil emailUtil;

    @Value("${project.book_sample}")
    private String path;

    @PostMapping("/create")
    public ResponseEntity<ResponseObject> createUser(@RequestBody UserDTO userDto) {
        ResponseObject responseObject = new ResponseObject();

        UserDTO userDTO = userService.createUser(userDto);

        responseObject.setMessage("User Created Successfully");
        responseObject.setStatus(true);
        responseObject.setData(userDTO);

        return new ResponseEntity<>(responseObject, HttpStatus.CREATED);
    }

    @PutMapping("/update/{userId}")
    public ResponseEntity<ResponseObject> updateUser(@RequestBody UserDTO userDto, @PathVariable String userId) {
        ResponseObject responseObject = new ResponseObject();

        UserDTO userDTO = userService.updateUser(userDto, userId);

        responseObject.setMessage("User With Id: " + userId + " is Updated Successfully");
        responseObject.setStatus(true);
        responseObject.setData(userDTO);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{userId}")
    public ResponseEntity<ResponseObject> deleteUser(@RequestBody UserDTO userDto, @PathVariable String userId) {
        ResponseObject responseObject = new ResponseObject();

        long deletedUser = userService.deleteUser(userDto, userId);

        responseObject.setMessage("User With Id: " + userId + " has been deleted Successfully");
        responseObject.setStatus(true);
        responseObject.setData(deletedUser);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<ResponseObject> getAllUsers() {
        ResponseObject responseObject = new ResponseObject();

        List<UserDTO> userDtoList = userService.getAllUsers();

        responseObject.setMessage("All Users Fetched");
        responseObject.setStatus(true);
        responseObject.setData(userDtoList);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<ResponseObject> getUserByUserId(@PathVariable String userId) {
        ResponseObject responseObject = new ResponseObject();

        UserDTO userDto = userService.findByUserId(userId);

        responseObject.setMessage("User With Id: " + userId + " Fetched Successfully");
        responseObject.setStatus(true);
        responseObject.setData(userDto);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/sendEmail/{userId}/{bookId}")
    public void sendEmailToUser(@PathVariable String userId, @PathVariable String bookId) throws MessagingException {
        String userEmail = userService.findByUserId(userId).getEmail();

        BookDTO bookDTO = bookService.findByBookId(bookId);

        if(bookDTO.getBook_sample() == "null") throw new ResourceNotFoundException("Book Sample", "BookId", bookId);

        GridFSFile gridFSFile = template.findOne(new Query().addCriteria(Criteria.where("_id").is(bookDTO.getBook_sample())));

        emailUtil.sendEmailToUser(userEmail, "This is a Book Sample of Book " + bookDTO.getName(), "This is a Book Sample", path + gridFSFile.getFilename());
    }
}
