package com.online.book.store.application.project.Exception;

public class ApiException extends RuntimeException{

    public ApiException(String message) {
        super(message);
    }
}
