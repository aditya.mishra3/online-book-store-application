package com.online.book.store.application.project.Repository;

import com.online.book.store.application.project.Model.Warehouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class WarehouseRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    public Warehouse createWarehouse(Warehouse warehouse) {
        return mongoTemplate.save(warehouse);
    }

    public long deleteWarehouse(Warehouse warehouse) {
        return mongoTemplate.remove(warehouse).getDeletedCount();
    }

    public List<Warehouse> getAllWarehouses() {
        List<Warehouse> books = mongoTemplate.findAll(Warehouse.class);
        return books;
    }

    public Optional<Warehouse> findByWarehouseId(String warehouseId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("warehouseId").is(warehouseId));
        return mongoTemplate.find(query, Warehouse.class).stream().findFirst();
    }

    public Optional<Warehouse> findByBookId(String bookId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("booksDetails.bookId").is(bookId));
        return mongoTemplate.find(query, Warehouse.class).stream().findFirst();
    }
}
