package com.online.book.store.application.project.Model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "warehouse_details")
public class Warehouse {

    @Id
    private String warehouseId;
    private String phone;
    private String address;
    private List<Warehouse_Book> booksDetails;

}
