package com.online.book.store.application.project.Impl;

import com.online.book.store.application.project.Dto.OwnerDTO;

import java.util.List;

public interface OwnerService {

    OwnerDTO createOwner(OwnerDTO ownerDTO);

    OwnerDTO updateOwner(OwnerDTO ownerDTO, String ownerId);

    long deleteOwner(OwnerDTO ownerDTO, String ownerId);

    List<OwnerDTO> getAllOwners();

    OwnerDTO findByOwnerId(String ownerId);
}
