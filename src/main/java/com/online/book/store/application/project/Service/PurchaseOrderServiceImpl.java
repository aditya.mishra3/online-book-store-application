package com.online.book.store.application.project.Service;

import com.online.book.store.application.project.Dto.PurchaseOrderDTO;
import com.online.book.store.application.project.Exception.BookOutOfStockException;
import com.online.book.store.application.project.Exception.ResourceNotFoundException;
import com.online.book.store.application.project.Impl.PurchaseOrderService;
import com.online.book.store.application.project.Model.Book;
import com.online.book.store.application.project.Model.PurchaseOrder;
import com.online.book.store.application.project.Model.PurchaseOrderInfoByPeriod;
import com.online.book.store.application.project.Model.Warehouse;
import com.online.book.store.application.project.Repository.BookRepository;
import com.online.book.store.application.project.Repository.PurchaseOrderInfoByPeriodRepository;
import com.online.book.store.application.project.Repository.PurchaseOrderRepository;
import com.online.book.store.application.project.Repository.WarehouseRepository;
import com.online.book.store.application.project.Util.FileExporterUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private PurchaseOrderInfoByPeriodRepository purchaseOrderInfoByPeriodRepository;

    @Autowired
    private FileExporterUtil fileExporterService;

    @Override
    public PurchaseOrderDTO createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO) {
        PurchaseOrder purchaseOrder = modelMapper.map(purchaseOrderDTO, PurchaseOrder.class);
        int totalPrice = 0;
        PurchaseOrder savedOrder = new PurchaseOrder();
        for (String bookId : purchaseOrder.getBookInfo()) {
            Book book = bookRepository.findByBookId(bookId).orElseThrow(() -> new ResourceNotFoundException("Book", "ID", bookId));
            if (book.isInStock()) {
                Warehouse warehouse = warehouseRepository.findByBookId(bookId).orElseThrow(() -> new ResourceNotFoundException("Book", "Warehouse", bookId));
                warehouse.getBooksDetails().stream().forEach((books) -> {
                    if (bookId.equalsIgnoreCase(books.getBookId())) {
                        books.setQuantity(books.getQuantity() - 1);
                        warehouseRepository.createWarehouse(warehouse);
                    }
                });
                totalPrice += book.getPrice();

                purchaseOrder.setOrderTotal(totalPrice);
                purchaseOrder.setOrderDate(System.currentTimeMillis());
                savedOrder = purchaseOrderRepository.createPurchaseOrder(purchaseOrder);
            }
            else {
                throw new BookOutOfStockException();
            }
        }

        return modelMapper.map(savedOrder, PurchaseOrderDTO.class);
    }

    @Override
    public long deletePurchaseOrder(PurchaseOrderDTO purchaseOrderDTO, String orderId) {
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findByPurchaseOrderId(orderId).orElseThrow(() -> new ResourceNotFoundException("Purchase Order", "ID", orderId));

        return purchaseOrderRepository.deletePurchaseOrder(purchaseOrder);
    }

    @Override
    public List<PurchaseOrderDTO> getAllPurchaseOrders() {
        List<PurchaseOrder> purchaseOrderList = purchaseOrderRepository.getAllPurchaseOrders();

        return purchaseOrderList.stream().map((orders) -> modelMapper.map(orders, PurchaseOrderDTO.class)).collect(Collectors.toList());
    }

    @Override
    public PurchaseOrderDTO findByPurchaseOrderId(String orderId) {
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findByPurchaseOrderId(orderId).orElseThrow(() -> new ResourceNotFoundException("Purchase Order", "ID", orderId));

        return modelMapper.map(purchaseOrder, PurchaseOrderDTO.class);
    }

    @Override
    public void getDailyPurchaseOrderData(HttpServletResponse response, int day, int month, int year) throws IOException {
        List<PurchaseOrderInfoByPeriod> dailyPurchaseOrderData = purchaseOrderInfoByPeriodRepository.getDailyPurchaseOrderData(day, month, year);
        fileExporterService.exportToExcel(dailyPurchaseOrderData, response);
    }

    @Override
    public void getWeeklyPurchaseOrderData(HttpServletResponse response, int weekNumber, int year) throws IOException {
        List<PurchaseOrderInfoByPeriod> weeklyPurchaseOrderData = purchaseOrderInfoByPeriodRepository.getWeeklyPurchaseOrderData(weekNumber, year);
        fileExporterService.exportToExcel(weeklyPurchaseOrderData, response);
    }

    @Override
    public void getMonthlyPurchaseOrderData(HttpServletResponse response, int month, int year) throws IOException {
        List<PurchaseOrderInfoByPeriod> monthlyPurchaseOrderData = purchaseOrderInfoByPeriodRepository.getMonthlyPurchaseOrderData(month, year);
        fileExporterService.exportToExcel(monthlyPurchaseOrderData, response);
    }

    @Override
    public void getYearlyPurchaseOrderData(HttpServletResponse response, int year) throws IOException {
        List<PurchaseOrderInfoByPeriod> yearlyPurchaseOrderData = purchaseOrderInfoByPeriodRepository.getYearlyPurchaseOrderData(year);
        fileExporterService.exportToExcel(yearlyPurchaseOrderData, response);
    }
}
