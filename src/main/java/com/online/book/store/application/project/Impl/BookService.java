package com.online.book.store.application.project.Impl;

import com.online.book.store.application.project.Dto.BookDTO;

import java.io.IOException;
import java.util.List;

public interface BookService {

    BookDTO createBook(BookDTO bookDTO);

    BookDTO updateBook(BookDTO bookDTO, String bookId);

    long deleteBook(BookDTO bookDTO, String bookId) throws IOException;

    List<BookDTO> getAllBooks();

    BookDTO findByBookId(String bookId);
}
