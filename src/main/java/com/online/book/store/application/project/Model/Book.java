package com.online.book.store.application.project.Model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "book_details")
public class Book {

    @Id
    private String bookId;
    private String author;
    private String name;
    private Integer price;
    private String book_sample;
    private boolean inStock;

}
