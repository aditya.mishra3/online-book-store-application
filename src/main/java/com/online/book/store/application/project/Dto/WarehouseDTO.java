package com.online.book.store.application.project.Dto;

import com.online.book.store.application.project.Model.Warehouse_Book;
import lombok.Data;

import java.util.List;

@Data
public class WarehouseDTO {

    private String warehouseId;
    private String phone;
    private String address;
    private List<Warehouse_Book> booksDetails;

}
