package com.online.book.store.application.project.Scheduler;

import com.online.book.store.application.project.Exception.ResourceNotFoundException;
import com.online.book.store.application.project.Model.Book;
import com.online.book.store.application.project.Model.Owner;
import com.online.book.store.application.project.Model.Warehouse;
import com.online.book.store.application.project.Model.Warehouse_Book;
import com.online.book.store.application.project.Repository.BookRepository;
import com.online.book.store.application.project.Repository.OwnerRepository;
import com.online.book.store.application.project.Repository.WarehouseRepository;
import com.online.book.store.application.project.Util.EmailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InventoryCheckScheduler {

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private EmailUtil emailUtil;

    @Scheduled(cron = "0 0 0 * * *")
    public void checkingWarehouseToGetLowQuantityOfAllBooks() {
        List<Warehouse> warehouseList = warehouseRepository.getAllWarehouses();

        List<Owner> ownerList = ownerRepository.getAllOwners();
        List<String> bookIds = new ArrayList<>();
        for (Warehouse warehouse : warehouseList) {
            for (Warehouse_Book warehouse_book : warehouse.getBooksDetails()) {
                if (warehouse_book.getBookId() != null && warehouse_book.getQuantity() <= 5) {
                    bookIds.add(warehouse_book.getBookId());
                }
            }
        }
        emailUtil.sendEmailToOwner(ownerList.get(0).getEmail(), "Book Low Quantity Alert", bookIds, ownerList.get(0).getName());
    }

    @Scheduled(cron = "0 */5 * * * *")
    public void checkForOutOfStockBooks() {
        List<Warehouse> warehouseList = warehouseRepository.getAllWarehouses();

        for (Warehouse warehouse : warehouseList) {
            for (Warehouse_Book warehouse_book : warehouse.getBooksDetails()) {
                if (warehouse_book.getBookId() != null && warehouse_book.getQuantity() <= 0) {
                    Book book = bookRepository.findByBookId(warehouse_book.getBookId()).orElseThrow(() -> new ResourceNotFoundException("Book", "ID", warehouse_book.getBookId()));
                    book.setInStock(false);
                    bookRepository.createBook(book);
                }
            }
        }
    }
}
