package com.online.book.store.application.project.Repository;

import com.online.book.store.application.project.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    public User createUser(User user) {
        return mongoTemplate.save(user);
    }

    public long deleteUser(User user) {
        return mongoTemplate.remove(user).getDeletedCount();
    }

    public List<User> getAllUsers() {
        List<User> users = mongoTemplate.findAll(User.class);
        return users;
    }

    public Optional<User> findByUserId(String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        return mongoTemplate.find(query, User.class).stream().findFirst();
    }

    public Optional<User> findByEmail(String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(email));
        return mongoTemplate.find(query, User.class).stream().findFirst();
    }
}
