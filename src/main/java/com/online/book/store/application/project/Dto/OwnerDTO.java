package com.online.book.store.application.project.Dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class OwnerDTO extends UserDTO{

    List<String> books = new ArrayList<>();
    List<String> warehouses = new ArrayList<>();

}
