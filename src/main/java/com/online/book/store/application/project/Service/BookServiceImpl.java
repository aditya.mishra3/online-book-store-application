package com.online.book.store.application.project.Service;

import com.online.book.store.application.project.Dto.BookDTO;
import com.online.book.store.application.project.Exception.ResourceNotFoundException;
import com.online.book.store.application.project.Impl.BookService;
import com.online.book.store.application.project.Model.Book;
import com.online.book.store.application.project.Model.Owner;
import com.online.book.store.application.project.Repository.BookRepository;
import com.online.book.store.application.project.Repository.OwnerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private GridFsTemplate template;

    @Value("${project.book_sample}")
    private String path;

    @Override
    public BookDTO createBook(BookDTO bookDTO) {
        Book book = modelMapper.map(bookDTO, Book.class);
        Book savedBook = bookRepository.createBook(book);
        BookDTO bookDto = modelMapper.map(savedBook, BookDTO.class);
        ownerRepository.getAllOwners().stream().forEach((owner) -> {
            owner.getBooks().add(bookDto.getBookId());
            ownerRepository.createOwner(owner);
        });
        return bookDto;
    }

    @Override
    public BookDTO updateBook(BookDTO bookDTO, String bookId) {
        Book book = bookRepository.findByBookId(bookId).orElseThrow(() -> new ResourceNotFoundException("Book", "ID", bookId));

        book.setAuthor(bookDTO.getAuthor());
        book.setName(bookDTO.getName());
        book.setPrice(bookDTO.getPrice());

        Book updatedBook = bookRepository.createBook(book);

        return modelMapper.map(updatedBook, BookDTO.class);
    }

    @Override
    public long deleteBook(BookDTO bookDTO, String bookId) throws IOException {
        Book book = bookRepository.findByBookId(bookId).orElseThrow(() -> new ResourceNotFoundException("Book", "ID", bookId));

        template.delete(new Query().addCriteria(Criteria.where("_id").is(book.getBook_sample())));
        Files.delete(Paths.get(path + book.getName() + ".pdf"));
        Owner owner = ownerRepository.getAllOwners().get(0);
        owner.getBooks().remove(bookId);
        ownerRepository.createOwner(owner);

        return bookRepository.deleteBook(book);
    }

    @Override
    public List<BookDTO> getAllBooks() {
        List<Book> bookList = bookRepository.getAllBooks();
        return bookList.stream().map((book) -> modelMapper.map(book, BookDTO.class)).collect(Collectors.toList());
    }

    @Override
    public BookDTO findByBookId(String bookId) {
        Book book = bookRepository.findByBookId(bookId).orElseThrow(() -> new ResourceNotFoundException("Book", "ID", bookId));
        return modelMapper.map(book, BookDTO.class);
    }
}
