package com.online.book.store.application.project.Controller;

import com.online.book.store.application.project.Dto.*;
import com.online.book.store.application.project.Exception.ApiException;
import com.online.book.store.application.project.Security.CustomUserDetailService;
import com.online.book.store.application.project.Security.JwtTokenHelper;
import com.online.book.store.application.project.Service.OwnerServiceImpl;
import com.online.book.store.application.project.Service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth/")
public class AuthController {

    @Autowired
    private JwtTokenHelper jwtTokenHelper;

    @Autowired
    private CustomUserDetailService customUserDetailService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private OwnerServiceImpl ownerService;

    @PostMapping("/login")
    public ResponseEntity<JwtResponseDTO> createToken(@RequestBody JwtRequestDTO jwtRequestDTO) throws Exception {

        authenticate(jwtRequestDTO.getUsername(), jwtRequestDTO.getPassword());

        UserDetails userDetails = customUserDetailService.loadUserByUsername(jwtRequestDTO.getUsername());

        String token = jwtTokenHelper.generateToken(userDetails);

        JwtResponseDTO jwtResponseDTO = new JwtResponseDTO();
        jwtResponseDTO.setToken(token);

        return new ResponseEntity<>(jwtResponseDTO, HttpStatus.CREATED);
    }

    private void authenticate(String username, String password) throws Exception {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(username,password);

        try {
            authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        }
        catch (BadCredentialsException e) {
            System.out.println("Invalid Details");
            throw new ApiException("Invalid username or password !!");
        }
    }

    @PostMapping("/sign-up/user")
    public ResponseEntity<ResponseObject> registerNewUser(@RequestBody UserDTO userDTO) {
        ResponseObject responseObject = new ResponseObject();

        UserDTO newUser = userService.createUser(userDTO);

        responseObject.setMessage("User Registered Successfully");
        responseObject.setStatus(true);
        responseObject.setData(newUser);

        return new ResponseEntity<>(responseObject, HttpStatus.CREATED);
    }

    @PostMapping("/sign-up/owner")
    public ResponseEntity<ResponseObject> registerNewOwner(@RequestBody OwnerDTO ownerDTO) {
        ResponseObject responseObject = new ResponseObject();

        OwnerDTO newOwner = ownerService.createOwner(ownerDTO);

        responseObject.setMessage("Owner Registered Successfully");
        responseObject.setStatus(true);
        responseObject.setData(newOwner);

        return new ResponseEntity<>(responseObject, HttpStatus.CREATED);
    }
}
