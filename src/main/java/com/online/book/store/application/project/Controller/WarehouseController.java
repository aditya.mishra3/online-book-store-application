package com.online.book.store.application.project.Controller;

import com.online.book.store.application.project.Dto.ResponseObject;
import com.online.book.store.application.project.Dto.WarehouseDTO;
import com.online.book.store.application.project.Exception.ResourceNotFoundException;
import com.online.book.store.application.project.Model.Warehouse;
import com.online.book.store.application.project.Repository.WarehouseRepository;
import com.online.book.store.application.project.Service.WarehouseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/warehouse")
public class WarehouseController {

    @Autowired
    private WarehouseServiceImpl warehouseService;

    @Autowired
    private WarehouseRepository warehouseRepository;

    @PostMapping("/create")
    public ResponseEntity<ResponseObject> createWarehouse(@RequestBody WarehouseDTO warehouseDTO) {
        ResponseObject responseObject = new ResponseObject();

        WarehouseDTO warehouseDto = warehouseService.createWarehouse(warehouseDTO);

        responseObject.setMessage("Warehouse Successfully Created");
        responseObject.setStatus(true);
        responseObject.setData(warehouseDTO);

        return new ResponseEntity<>(responseObject, HttpStatus.CREATED);
    }

    @PutMapping("/update/{warehouseId}")
    public ResponseEntity<ResponseObject> updateWarehouse(@RequestBody WarehouseDTO warehouseDTO, @PathVariable String warehouseId) {
        ResponseObject responseObject = new ResponseObject();

        WarehouseDTO warehouseDto = warehouseService.updateWarehouse(warehouseDTO, warehouseId);

        responseObject.setMessage("Warehouse With Id: " + warehouseId + " Updated Successfully");
        responseObject.setStatus(true);
        responseObject.setData(warehouseDto);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{warehouseId}")
    public ResponseEntity<ResponseObject> deleteWarehouse(@RequestBody WarehouseDTO warehouseDTO, @PathVariable String warehouseId) {
        ResponseObject responseObject = new ResponseObject();

        long deletedWarehouse = warehouseService.deleteWarehouse(warehouseDTO, warehouseId);

        responseObject.setMessage("Warehouse With Id: " + warehouseId + " Deleted Successfully");
        responseObject.setStatus(true);
        responseObject.setData(deletedWarehouse);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<ResponseObject> getAllWarehouses() {
        ResponseObject responseObject = new ResponseObject();

        List<WarehouseDTO> warehouseDTOList = warehouseService.getAllWarehouse();

        responseObject.setMessage("All Warehouse Fetched");
        responseObject.setStatus(true);
        responseObject.setData(warehouseDTOList);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/{warehouseId}")
    public ResponseEntity<ResponseObject> getWarehouseByWarehouseId(@PathVariable String warehouseId) {
        ResponseObject responseObject = new ResponseObject();

        WarehouseDTO warehouseDTO = warehouseService.findByWarehouseId(warehouseId);

        responseObject.setMessage("Warehouse With Id: " + warehouseId + " Fetched Successfully");
        responseObject.setStatus(true);
        responseObject.setData(warehouseDTO);

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
