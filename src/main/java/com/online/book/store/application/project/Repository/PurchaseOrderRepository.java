package com.online.book.store.application.project.Repository;

import com.online.book.store.application.project.Model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseOrderRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    public PurchaseOrder createPurchaseOrder(PurchaseOrder purchaseOrder) {
        return mongoTemplate.save(purchaseOrder);
    }

    public long deletePurchaseOrder(PurchaseOrder purchaseOrder) {
        return mongoTemplate.remove(purchaseOrder).getDeletedCount();
    }

    public List<PurchaseOrder> getAllPurchaseOrders() {
        List<PurchaseOrder> purchaseOrders = mongoTemplate.findAll(PurchaseOrder.class);
        return purchaseOrders;
    }

    public Optional<PurchaseOrder> findByPurchaseOrderId(String orderId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("orderId").is(orderId));
        return mongoTemplate.find(query, PurchaseOrder.class).stream().findFirst();
    }

    public List<PurchaseOrder> findPurchaseOrderBetweenOrderDate(long startDate, long endDate) {
        Query query = new Query();
        query.addCriteria(Criteria.where("orderDate").lt(endDate).gt(startDate));
        return mongoTemplate.find(query, PurchaseOrder.class);
    }
}
