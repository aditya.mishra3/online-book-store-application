package com.online.book.store.application.project.Util;

import com.online.book.store.application.project.Model.PurchaseOrderInfoByPeriod;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class FileExporterUtil {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    public void exportToExcel(List<PurchaseOrderInfoByPeriod> purchaseOrderData, HttpServletResponse response) throws IOException {
        workbook = new XSSFWorkbook();
        setResponseHeader(response, "application/octet-stream", ".xlsx", "Purchase_order_details_");

        writeHeaderLine();
        writeDataLine(purchaseOrderData);

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Purchase_order_details");
        XSSFRow row = sheet.createRow(0);
        XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        cellStyle.setFont(font);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);

        createCell(row, 0, "S.No", cellStyle);
        createCell(row, 1, "User Name", cellStyle);
        createCell(row, 2, "Total Amount", cellStyle);
        createCell(row, 3, "Book Name", cellStyle);
    }

    private void createCell(XSSFRow row, int columnIndex, Object value, CellStyle cellStyle) {
        XSSFCell cell = row.createCell(columnIndex);
        sheet.autoSizeColumn(columnIndex);
        if(value instanceof Integer)
            cell.setCellValue((Integer) value);
        else if(value instanceof Boolean)
            cell.setCellValue((Boolean) value);
        else
            cell.setCellValue((String) value);

        cell.setCellStyle(cellStyle);
    }

    private void writeDataLine(List<PurchaseOrderInfoByPeriod> purchaseOrderList) {
        int rowIndex = 1;

        XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        cellStyle.setFont(font);
        cellStyle.setAlignment(HorizontalAlignment.LEFT);

        for (PurchaseOrderInfoByPeriod purchaseOrder : purchaseOrderList) {
            XSSFRow row = sheet.createRow(rowIndex++);
            int columnIndex = 0;
            createCell(row, columnIndex++, purchaseOrder.getId(), cellStyle);
            createCell(row, columnIndex++, purchaseOrder.getUserName(), cellStyle);
            createCell(row, columnIndex++, purchaseOrder.getTotalAmount(), cellStyle);
            for (String bookName : purchaseOrder.getBookName()) {
                createCell(row, columnIndex++, bookName, cellStyle);
            }
        }
    }

    private void setResponseHeader(HttpServletResponse response, String contentType, String extension, String prefix) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String timestamp = dateFormat.format(new Date());
        String fileName = prefix + timestamp + extension;

        response.setContentType(contentType);

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=" + fileName;
        response.setHeader(headerKey,headerValue);
    }
}
