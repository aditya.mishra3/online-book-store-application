package com.online.book.store.application.project;

import com.online.book.store.application.project.Model.Owner;
import com.online.book.store.application.project.Model.Role;
import com.online.book.store.application.project.Repository.OwnerRepository;
import com.online.book.store.application.project.Repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class OnlineBookStoreApplication implements CommandLineRunner {

	@Autowired
	private RoleRepository roleRepository;

	public static void main(String[] args) {
		SpringApplication.run(OnlineBookStoreApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		try {
			Role role1 = new Role();
			role1.setId(70);
			role1.setName("ROLE_ADMIN");
			roleRepository.createRole(role1);

			Role role2 = new Role();
			role2.setId(71);
			role2.setName("ROLE_NORMAL");
			roleRepository.createRole(role2);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
