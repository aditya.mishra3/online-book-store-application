package com.online.book.store.application.project.Impl;

import com.online.book.store.application.project.Model.BookSampleFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface BookSampleFileService {

    String uploadAndUpdateBookSampleFile(MultipartFile multipartFile, String path) throws IOException;

    BookSampleFile downloadBookSampleFile(String id) throws IOException;
}
