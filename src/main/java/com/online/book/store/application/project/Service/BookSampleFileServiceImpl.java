package com.online.book.store.application.project.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.online.book.store.application.project.Exception.FileTypeIncorrectException;
import com.online.book.store.application.project.Exception.ResourceNotFoundException;
import com.online.book.store.application.project.Impl.BookSampleFileService;
import com.online.book.store.application.project.Model.Book;
import com.online.book.store.application.project.Model.BookSampleFile;
import com.online.book.store.application.project.Repository.BookRepository;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class BookSampleFileServiceImpl implements BookSampleFileService {

    @Autowired
    private GridFsTemplate template;

    @Autowired
    private GridFsOperations operations;

    @Autowired
    private BookRepository bookRepository;

    @Override
    public String uploadAndUpdateBookSampleFile(MultipartFile multipartFile, String path) throws IOException {
       if(!multipartFile.getContentType().equalsIgnoreCase("application/pdf")) {
           throw new FileTypeIncorrectException();
       }

        DBObject metadata = new BasicDBObject();
        metadata.put("fileSize", multipartFile.getSize());

       int endIndex = multipartFile.getOriginalFilename().indexOf('.');
       String fileName = multipartFile.getOriginalFilename().substring(0,endIndex);

        Book book = bookRepository.findByBookName(fileName).orElseThrow(() -> new ResourceNotFoundException("Book", "Name", fileName));

        Object pdfId;

        if (book.getBook_sample() == "null") {

            pdfId = template.store(multipartFile.getInputStream(),multipartFile.getOriginalFilename(),multipartFile.getContentType(),metadata);
        }
        else {
            template.delete(new Query().addCriteria(Criteria.where("filename").is(multipartFile.getOriginalFilename())));
            Files.delete(Paths.get(path + multipartFile.getOriginalFilename()));

            pdfId = template.store(multipartFile.getInputStream(),multipartFile.getOriginalFilename(),multipartFile.getContentType(),metadata);
        }

        book.setBook_sample(pdfId.toString());
        bookRepository.createBook(book);


        File file1 = new File(path);
        if(!file1.exists()) {
            file1.mkdir();
        }

        Files.copy(multipartFile.getInputStream(), Paths.get(path + multipartFile.getOriginalFilename()));

        return pdfId.toString();
    }

    @Override
    public BookSampleFile downloadBookSampleFile(String id) throws IOException {
        GridFSFile gridFSFile = template.findOne(new Query(Criteria.where("_id").is(id)));

        BookSampleFile loadFile = new BookSampleFile();

        if (gridFSFile != null && gridFSFile.getMetadata() != null) {
            loadFile.setFilename(gridFSFile.getFilename());
            loadFile.setFileType(gridFSFile.getMetadata().get("_contentType").toString());
            loadFile.setFileSize(gridFSFile.getMetadata().get("fileSize").toString());
            loadFile.setFile(IOUtils.toByteArray(operations.getResource(gridFSFile).getInputStream()));
        }

        return loadFile;
    }
}
