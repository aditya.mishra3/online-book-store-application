package com.online.book.store.application.project.Model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@Document(collection = "owner_details")
public class Owner extends User{

    List<String> books = new ArrayList<>();
    List<String> warehouses = new ArrayList<>();

}
