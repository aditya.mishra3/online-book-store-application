package com.online.book.store.application.project.Security;

import com.online.book.store.application.project.Exception.ResourceNotFoundException;
import com.online.book.store.application.project.Model.Owner;
import com.online.book.store.application.project.Model.User;
import com.online.book.store.application.project.Repository.OwnerRepository;
import com.online.book.store.application.project.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OwnerRepository ownerRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByEmail(username);
        if(user.isPresent()) {
            return user.get();
        }
        Owner owner = ownerRepository.findByEmail(username).orElseThrow(() -> new ResourceNotFoundException("User and Owner", "Email", username));
        return owner;
    }
}
